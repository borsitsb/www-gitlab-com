---
layout: handbook-page-toc
title: "Speaker Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Speaker Resources

The Developer Evangelism team is always happy to help at any stage of talk preparation, there is also a [speakers resources page](/handbook/marketing/corporate-marketing/speaking-resources/) where you can find helpful guides and tips. 

## Speakers Coffee Chats

The Developer Evangelism team will be hosting monthly coffee chats where new and experienced speakers can join in to discuss ideas of stories they want to share, pitch talks for feedback and seek help with preparing presentations. These are Zoom sessions, open to the GitLab community and it will be streamed on youtube.

Special sessions will also be organized for prominent conferences like KubeCon to help more people get quality talks created. See the team calendar before for dates.

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>


